# libreverse

An multiplayer multi-use game platform.

## Goals

- provide a platform to make minigames in
- simple protocol
- blazing fast and highly configurable 🚀🚀🚀

## Features / Todo

- [x] Spawn entities
- [x] Transmit player location
- [x] Positional audio for mumble
- [x] Send incremental updates to clients
- [ ] Index entities with a quadtree
- [ ] Minigames as plugins (wasm, lua or cdylib)
- [ ] Player inventory

## License

GNU Affero General Public License version 3 only, see [LICENSE](./LICENSE)
