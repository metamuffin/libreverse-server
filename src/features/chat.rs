use crate::{ecswrapper::World, Dialog, Lifetime, Name, Parented};
use hecs::Entity;

pub struct ChatServerRef(pub Entity);

pub fn init(world: &mut World) {
    let s = world.spawn((Name("Server".to_string()),));
    world.resources.insert(ChatServerRef(s))
}

pub fn chat_server_notice(world: &mut World, mesg: String) {
    world.spawn((
        Dialog(mesg),
        Parented(world.resources.get::<ChatServerRef>().unwrap().0),
    ));
}

pub fn chat_send_message(world: &mut World, sender: Entity, mesg: String) {
    world.spawn((
        Parented(sender),
        Lifetime(1.0 + mesg.len() as f64 * 0.05),
        Dialog(mesg.clone()),
    ));
}
