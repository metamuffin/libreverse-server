use crate::{ecswrapper::World, Lifetime};

pub fn update(world: &mut World) {
    let mut expired = vec![];

    for (e, l) in world.query_mut::<&mut Lifetime>() {
        l.0 -= 1.0 / 30.0;
        if l.0 < 0.0 {
            expired.push(e)
        }
    }

    for e in expired {
        world.despawn(e).unwrap();
    }
}
