pub mod inbound;
pub mod lifetime;
pub mod outbound;
pub mod tilemap;
pub mod testing;
pub mod chat;

use crate::ecswrapper::World;

pub fn init_systems(world: &mut World) {
    tilemap::init(world);
    chat::init(world)
}

pub fn run_systems(world: &mut World) {
    lifetime::update(world);
    outbound::update(world);
    testing::update(world)
}
