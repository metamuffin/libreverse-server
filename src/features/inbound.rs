use super::chat::chat_send_message;
use crate::{
    ecswrapper::World,
    protocol::{serialize_entity, ClientboundPacket, ServerboundPacket},
    Camera, Name, Player, PlayerOwned, Position, Size, Texture,
};
use crossbeam_channel::Sender;
use log::info;

pub fn dispath_inbound_packet(
    world: &mut World,
    client_id: u32,
    packet: ServerboundPacket,
    responder: Sender<ClientboundPacket>,
) {
    match packet {
        ServerboundPacket::Quit => {
            let mut q = vec![];
            for (e, ownership) in &mut world.query::<&PlayerOwned>() {
                if ownership.0 == client_id {
                    q.push(e);
                }
            }
            info!(
                "{} entities were despawned, because {client_id:} quit",
                q.len(),
            );
            for e in q {
                world.despawn(e).unwrap();
            }
        }
        ServerboundPacket::Login { name } => {
            let _ = responder.send(ClientboundPacket::Info {
                player_id: client_id,
            });
            for (e, _) in &mut world.query::<()>() {
                let _ = responder.send(ClientboundPacket::UpdateEntity {
                    id: e.id(),
                    data: serialize_entity(world.entity(e).unwrap(), None),
                });
            }
            world.spawn((
                Player(client_id),
                Position(0.0, 0.0),
                Size(32.0, 32.0),
                Name(name),
                PlayerOwned(client_id),
                Texture("https://s.metamuffin.org/media/libreverse/rocket.png".to_string()),
                Camera(client_id, responder),
            ));
        }
        ServerboundPacket::Movement { x, y } => {
            for e in world.query_eq::<Player>(Player(client_id)) {
                let mut pos = world.get_mut::<Position>(e).unwrap();
                pos.0 += x;
                pos.1 += y;
            }
        }
        ServerboundPacket::Chat { message } => {
            for e in world.query_eq::<Player>(Player(client_id)) {
                chat_send_message(world, e, message.clone())
            }
        }
    }
}
