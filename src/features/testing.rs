use crate::{ecswrapper::World, Player, Position};

use super::tilemap::{TileChunk, TILEMAP_CHUNK_SIZE};

pub fn update(world: &mut World) {
    for (_player_entity, (_player, pos)) in &mut world.query::<(&Player, &Position)>() {
        // println!("{:?}", world.quadtree);
        for e in world.query_overlap(*pos) {
            // println!("overlap");
            if let Some(mut chunk) = world.get_mut::<&mut TileChunk>(e) {
                // println!("chunk");
                if let Some(p) = world.get::<&Position>(e) {
                    // println!("pos");
                    let relpos = (*pos - **p) / Position(16.0, 16.0)
                        - Position(
                            TILEMAP_CHUNK_SIZE as f64 * 0.5,
                            TILEMAP_CHUNK_SIZE as f64 * 0.5,
                        );
                    let (x, y) = (relpos.0 as usize, relpos.1 as usize);
                    chunk.data[x][y] = 3
                }
            }
        }
    }
}
