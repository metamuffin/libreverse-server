use std::collections::BTreeMap;

use serde::Serialize;

use crate::{ecswrapper::World, Position, Size};

pub const TILEMAP_CHUNK_SIZE: usize = 16;

#[derive(Debug, Serialize)]
pub struct TileChunk {
    pub data: [[usize; TILEMAP_CHUNK_SIZE]; TILEMAP_CHUNK_SIZE],
}

#[derive(Debug, Clone, Serialize)]
pub struct Tilemap {
    pub tile_size: f64,
    pub chunk_size: usize,
    pub textures: BTreeMap<usize, String>,
}

pub fn init(world: &mut World) {
    #[rustfmt::skip]
    let textures = BTreeMap::from_iter([
        (0, "https://s.metamuffin.org/media/libreverse/tiles/tile_0042.png".to_string()),
        (1, "https://s.metamuffin.org/media/libreverse/tiles/tile_0056.png".to_string()),
        (2, "https://s.metamuffin.org/media/libreverse/tiles/tile_0056.png".to_string()),
        (3, "https://s.metamuffin.org/media/libreverse/tiles/tile_0072.png".to_string()),
    ]);

    let mut data = [[0; TILEMAP_CHUNK_SIZE]; TILEMAP_CHUNK_SIZE];
    data[1][1] = 1;
    data[1][2] = 1;
    data[1][3] = 1;
    data[1][4] = 1;
    data[2][4] = 1;

    let tile_size = 16.0;

    world.spawn((Tilemap {
        chunk_size: TILEMAP_CHUNK_SIZE,
        tile_size,
        textures,
    },));

    world.spawn((
        TileChunk { data },
        Position(0.0, 0.0),
        Size(
            TILEMAP_CHUNK_SIZE as f64 * tile_size,
            TILEMAP_CHUNK_SIZE as f64 * tile_size,
        ),
    ));
}
