use crate::{
    ecswrapper::World,
    protocol::{proto_component_name, serialize_entity, ClientboundPacket},
    Camera,
};

pub fn update(world: &mut World) {
    let (mutated, destroyed) = world.delta();
    for (_, camera) in &mut world.query::<&Camera>() {
        for (e, c) in &destroyed {
            match c {
                Some(c) => {
                    let mut m = serde_json::Map::new();
                    m.insert(
                        proto_component_name(*c).to_string(),
                        serde_json::Value::Null,
                    );
                    let _ = camera.1.send(ClientboundPacket::UpdateEntity {
                        id: e.id(),
                        data: serde_json::Value::Object(m),
                    });
                }
                None => {
                    let _ = camera
                        .1
                        .send(ClientboundPacket::DestroyEntity { id: e.id() });
                }
            }
        }
        for (e, c) in &mutated {
            if let Ok(eref) = world.entity(*e) {
                let _ = camera.1.send(ClientboundPacket::UpdateEntity {
                    id: e.id(),
                    data: serialize_entity(eref, c.to_owned()),
                });
            }
        }
    }
}
