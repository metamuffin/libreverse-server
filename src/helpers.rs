pub mod bounds {
    use crate::{Position, Size};
    pub trait Bounds {
        fn position(&self) -> Position;
        fn size(&self) -> Size;

        fn overlaps(&self, rhs: impl Bounds) -> bool {
            let (l1, t1, r1, b1) = self.ltrb();
            let (l2, t2, r2, b2) = rhs.ltrb();
            l2 < r1 && r2 > l1 && t2 < b1 && b2 > t1
        }
        fn contains(&self, rhs: impl Bounds) -> bool {
            let (l1, t1, r1, b1) = self.ltrb();
            let (l2, t2, r2, b2) = rhs.ltrb();
            l1 < l2 && r2 < r1 && t1 < t2 && b2 < b1
        }
        fn ltrb(&self) -> (f64, f64, f64, f64) {
            let s = self.size();
            let p = self.position();
            (
                p.0 - s.0 * 0.5,
                p.1 - s.1 * 0.5,
                p.0 + s.0 * 0.5,
                p.1 + s.1 * 0.5,
            )
        }
    }

    impl Bounds for Position {
        fn position(&self) -> Position {
            *self
        }

        fn size(&self) -> Size {
            Size(0.0, 0.0)
        }
    }

    impl Bounds for (Position, Size) {
        fn position(&self) -> Position {
            self.0
        }

        fn size(&self) -> Size {
            self.1
        }
    }
}

pub mod pos_helper {
    use crate::{Position, Size};

    impl Position {
        pub fn to_size(self) -> Size {
            Size(self.0, self.1)
        }
    }
    impl Size {
        pub fn to_position(self) -> Position {
            Position(self.0, self.1)
        }
    }

    impl Position {
        pub fn flip(self) -> Self {
            Self(self.0 * -1.0, self.1 * -1.0)
        }
        pub fn flip_x(self) -> Self {
            Self(self.0 * -1.0, self.1)
        }
        pub fn flip_y(self) -> Self {
            Self(self.0, self.1 * -1.0)
        }
    }
}

pub mod pos_arith {
    use crate::Position;

    impl std::ops::Add for Position {
        type Output = Position;

        fn add(self, rhs: Self) -> Self::Output {
            Position(self.0 + rhs.0, self.1 + rhs.1)
        }
    }

    impl std::ops::Sub for Position {
        type Output = Position;

        fn sub(self, rhs: Self) -> Self::Output {
            Position(self.0 - rhs.0, self.1 - rhs.1)
        }
    }

    impl std::ops::Mul for Position {
        type Output = Position;

        fn mul(self, rhs: Self) -> Self::Output {
            Position(self.0 * rhs.0, self.1 * rhs.1)
        }
    }

    impl std::ops::Div for Position {
        type Output = Position;

        fn div(self, rhs: Self) -> Self::Output {
            Position(self.0 / rhs.0, self.1 / rhs.1)
        }
    }
}
pub mod size_arith {
    use crate::Size;

    impl std::ops::Add for Size {
        type Output = Size;

        fn add(self, rhs: Self) -> Self::Output {
            Size(self.0 + rhs.0, self.1 + rhs.1)
        }
    }

    impl std::ops::Sub for Size {
        type Output = Size;

        fn sub(self, rhs: Self) -> Self::Output {
            Size(self.0 - rhs.0, self.1 - rhs.1)
        }
    }

    impl std::ops::Mul for Size {
        type Output = Size;

        fn mul(self, rhs: Self) -> Self::Output {
            Size(self.0 * rhs.0, self.1 * rhs.1)
        }
    }

    impl std::ops::Div for Size {
        type Output = Size;

        fn div(self, rhs: Self) -> Self::Output {
            Size(self.0 / rhs.0, self.1 / rhs.1)
        }
    }
}
