#![feature(box_syntax)]

use crossbeam_channel::{Receiver, Sender};
use libreadventure::{
    ecswrapper::World,
    features::{inbound::dispath_inbound_packet, init_systems, run_systems},
    network::network_loop,
    protocol::{ClientboundPacket, ServerboundPacket},
};
use log::info;
use std::{
    thread,
    time::{Duration, Instant},
};

fn main() {
    env_logger::init();

    let (s, r) = crossbeam_channel::unbounded();
    thread::spawn(move || network_loop(s));
    update_loop(r);
}

fn update_loop(packets: Receiver<(u32, ServerboundPacket, Sender<ClientboundPacket>)>) {
    let mut world = World::new();

    init_systems(&mut world);

    let start = Instant::now();
    let mut update_counter = 0;
    let mut avr_update_duration = 1.0;
    loop {
        let perf_start = Instant::now();

        for (client_id, packet, responder) in packets.try_iter() {
            dispath_inbound_packet(&mut world, client_id, packet, responder)
        }
        run_systems(&mut world);

        let update_duration = perf_start.elapsed().as_secs_f64();
        avr_update_duration = (update_duration - avr_update_duration) * 0.01;
        update_counter += 1;

        let end_delta = (1.0 / 30.0) * update_counter as f64 - start.elapsed().as_secs_f64();
        if end_delta > 0.0 {
            thread::sleep(Duration::from_secs_f64(end_delta));
        }

        if update_counter % 30 * 10 == 0 {
            info!(
                "avr update: {:.2}ns, load: {:.4}%",
                avr_update_duration * 1000000.0,
                avr_update_duration / (1.0 / 30.0) * 100.0
            )
        }
    }
}
