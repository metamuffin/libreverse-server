#![feature(box_syntax)]
#![feature(const_type_id)]

use crossbeam_channel::Sender;
use hecs::Entity;
use protocol::ClientboundPacket;
use serde::{Deserialize, Serialize, Serializer};
pub mod ecswrapper;
pub mod features;
pub mod network;
pub mod protocol;
pub mod quadtree;
pub mod helpers;

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct Player(pub u32);

/// indicates that this entity should be removed, after the player quits
#[derive(Debug, PartialEq, Eq)]
pub struct PlayerOwned(pub u32);

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct Name(pub String);

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub struct Position(pub f64, pub f64);

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub struct Size(pub f64, pub f64);

#[derive(Debug, Serialize, Deserialize)]
pub struct Texture(pub String);

#[derive(Debug, Serialize, Deserialize)]
pub struct Dialog(pub String);

#[derive(Debug, Serialize)]
pub struct Parented(#[serde(serialize_with = "ser_ent")] pub Entity);

#[derive(Debug)]
pub struct Lifetime(pub f64);

#[derive(Debug, Serialize)]
#[serde(transparent)]
pub struct Camera(
    pub u32,
    #[serde(skip_serializing)] pub Sender<ClientboundPacket>,
);

fn ser_ent<S>(e: &Entity, ser: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    ser.serialize_u32(e.id())
}
