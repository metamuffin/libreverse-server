use crate::helpers::bounds::Bounds;
use crate::{protocol::is_serializable, quadtree::Quad, Position};
use hecs::{
    Component, ComponentError, DynamicBundle, Entity, EntityRef, Fetch, NoSuchEntity, Query,
    QueryBorrow, QueryOne, Ref, RefMut,
};
use log::debug;
use std::any::Any;
use std::collections::BTreeMap;
use std::fmt::Debug;
use std::{any::TypeId, collections::BTreeSet, sync::Mutex};

pub struct World {
    inner: hecs::World,
    pub resources: Resources,
    pub quadtree: Quad,
    mutated: Mutex<BTreeSet<(Entity, Option<TypeId>)>>,
    destroyed: Mutex<Vec<(Entity, Option<TypeId>)>>,
}

impl World {
    pub fn new() -> Self {
        Self {
            inner: hecs::World::new(),
            quadtree: Quad::new_root(),
            mutated: Default::default(),
            destroyed: Default::default(),
            resources: Default::default(),
        }
    }

    pub fn delta(&mut self) -> (Vec<(Entity, Option<TypeId>)>, Vec<(Entity, Option<TypeId>)>) {
        let mut lock = self.mutated.lock().unwrap();
        let updated = Vec::from_iter(lock.iter().map(|&a| a));
        lock.clear();
        let mut lock = self.destroyed.lock().unwrap();
        let destroyed = Vec::from_iter(lock.iter().map(|&a| a));
        lock.clear();
        return (updated, destroyed);
    }

    pub fn spawn<D: DynamicBundle + Debug>(&mut self, bundle: D) -> Entity {
        debug!("spawn {:?}", bundle);
        let e = self.inner.spawn(bundle);
        if let Some(_) = self.inner.get::<Position>(e).ok() {
            self.quadtree.add(&self.inner, e);
        }
        self.mutated.lock().unwrap().insert((e, None));
        e
    }

    pub fn remove_one<C: Component>(&mut self, e: Entity) -> Result<C, ComponentError> {
        self.destroyed
            .lock()
            .unwrap()
            .push((e, Some(TypeId::of::<C>())));
        self.inner.remove_one::<C>(e)
    }

    pub fn despawn(&mut self, e: Entity) -> Result<(), NoSuchEntity> {
        self.destroyed.lock().unwrap().push((e, None));
        self.inner.despawn(e)
    }

    pub fn get<Q: Component + ImmutableQuery>(&self, e: Entity) -> Option<Ref<'_, Q>> {
        self.inner.get(e).ok()
    }

    pub fn get_mut<Q: Component>(&self, e: Entity) -> Option<RefMut<'_, Q>> {
        self.mutated
            .lock()
            .unwrap()
            .insert((e, Some(TypeId::of::<Q>())));
        self.inner.get_mut(e).ok()
    }

    // TODO accept mutable queries but still save mutations (fun fact: this is impossible)
    pub fn query<Q: Query + ImmutableQuery>(&self) -> QueryBorrow<Q> {
        self.inner.query::<Q>()
    }

    pub fn query_mut<Q: Query + TypeMeta>(
        &mut self,
    ) -> impl Iterator<Item = (Entity, <<Q as Query>::Fetch as Fetch<'_>>::Item)> {
        self.inner.query_mut::<Q>().into_iter().map(|e| {
            for t in Q::iter_mut_types() {
                if is_serializable(t) {
                    self.mutated.lock().unwrap().insert((e.0, Some(t)));
                }
            }
            return e;
        })
    }

    pub fn query_eq<Q: Component + PartialEq>(&mut self, val: Q) -> Vec<Entity> {
        let mut v = vec![];
        let mut i = self.inner.query::<&Q>();
        for (e, _) in &mut i {
            if *self.inner.get::<Q>(e).unwrap() == val {
                v.push(e)
            }
        }
        v
    }

    pub fn entity(&self, e: Entity) -> Result<EntityRef, NoSuchEntity> {
        self.inner.entity(e)
    }

    pub fn query_overlap<'a>(
        &'a self,
        bounds: impl Bounds + 'a + Copy,
    ) -> Box<dyn Iterator<Item = Entity> + '_> {
        self.quadtree.query_overlap(bounds)
    }

    pub fn query_one<Q: Query + ImmutableQuery>(
        &self,
        entity: Entity,
    ) -> Result<QueryOne<'_, Q>, NoSuchEntity> {
        self.inner.query_one(entity)
    }
}

#[derive(Debug, Default)]
pub struct Resources {
    data: BTreeMap<TypeId, Box<dyn Any>>,
}

impl Resources {
    pub fn get<R: Component>(&self) -> Option<&R> {
        self.data
            .get(&TypeId::of::<R>())
            .map(|e| e.downcast_ref::<R>().unwrap())
    }
    pub fn get_mut<R: Component>(&mut self) -> Option<&mut R> {
        self.data
            .get_mut(&TypeId::of::<R>())
            .map(|e| e.downcast_mut::<R>().unwrap())
    }
    pub fn insert<R: Component>(&mut self, r: R) {
        self.data.insert(TypeId::of::<R>(), box r);
    }
}

pub trait ImmutableQuery {}
impl<A: ImmutableQuery, B: ImmutableQuery> ImmutableQuery for (A, B) {}
impl<A: ImmutableQuery, B: ImmutableQuery, C: ImmutableQuery> ImmutableQuery for (A, B, C) {}
impl<T> ImmutableQuery for &T {}
impl ImmutableQuery for () {}

pub trait TypeMeta {
    const HAS_MUTABLE: bool;
    fn iter_types() -> Box<dyn Iterator<Item = TypeId>>;
    fn iter_mut_types() -> Box<dyn Iterator<Item = TypeId>>;
}
impl<A: TypeMeta, B: TypeMeta> TypeMeta for (A, B) {
    const HAS_MUTABLE: bool = A::HAS_MUTABLE || B::HAS_MUTABLE;
    fn iter_types() -> Box<dyn Iterator<Item = TypeId>> {
        box A::iter_types().chain(B::iter_types())
    }
    fn iter_mut_types() -> Box<dyn Iterator<Item = TypeId>> {
        box A::iter_types().chain(B::iter_types())
    }
}
impl TypeMeta for () {
    const HAS_MUTABLE: bool = false;
    fn iter_types() -> Box<dyn Iterator<Item = TypeId>> {
        box [].into_iter()
    }
    fn iter_mut_types() -> Box<dyn Iterator<Item = TypeId>> {
        box [].into_iter()
    }
}
impl<T: 'static> TypeMeta for &T {
    const HAS_MUTABLE: bool = false;
    fn iter_types() -> Box<dyn Iterator<Item = TypeId>> {
        box [TypeId::of::<T>()].into_iter()
    }
    fn iter_mut_types() -> Box<dyn Iterator<Item = TypeId>> {
        box [].into_iter()
    }
}
impl<T: 'static> TypeMeta for &mut T {
    const HAS_MUTABLE: bool = true;
    fn iter_types() -> Box<dyn Iterator<Item = TypeId>> {
        box [TypeId::of::<T>()].into_iter()
    }
    fn iter_mut_types() -> Box<dyn Iterator<Item = TypeId>> {
        box [TypeId::of::<T>()].into_iter()
    }
}
