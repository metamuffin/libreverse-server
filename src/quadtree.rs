use crate::{helpers::bounds::Bounds, Position, Size};
use hecs::Entity;
use log::debug;

const MAX_ENTS: usize = 10;
// const MIN_ENTS: usize = 5;

#[derive(Debug)]
pub struct Quad {
    bounds: (Position, Size),
    entities: Vec<(Entity, Position, Size)>,
    quads: Option<[[Box<Quad>; 2]; 2]>,
}

impl Quad {
    pub fn new_root() -> Self {
        Self::at((Position(0.0, 0.0), Size(2000.0, 2000.0)))
    }
    pub fn at(bounds: (Position, Size)) -> Self {
        Quad {
            bounds,
            entities: vec![],
            quads: None,
        }
    }

    pub fn add(&mut self, world: &hecs::World, e: Entity) {
        debug!("add {:?}", e);
        let (pos, size) = get_bounds(world, e);
        self.add_inner(e, pos, size);
    }

    pub fn remove(&mut self, world: &hecs::World, e: Entity) {
        debug!("remove {:?}", e);
        let (pos, size) = get_bounds(world, e);
        self.remove_inner(e, pos, size)
    }

    fn add_inner(&mut self, e: Entity, pos: Position, size: Size) {
        self.entities.push((e, pos, size));
        // TODO subdivide

        if self.entities.len() > MAX_ENTS && self.quads.is_none() {
            let quad_bounds = [
                [
                    (
                        self.bounds.position() + self.bounds.size().to_position().flip(),
                        self.bounds.size() * Size(0.5, 0.5),
                    ),
                    (
                        self.bounds.position() + self.bounds.size().to_position().flip_y(),
                        self.bounds.size() * Size(0.5, 0.5),
                    ),
                ],
                [
                    (
                        self.bounds.position() + self.bounds.size().to_position().flip_x(),
                        self.bounds.size() * Size(0.5, 0.5),
                    ),
                    (
                        self.bounds.position() + self.bounds.size().to_position(),
                        self.bounds.size() * Size(0.5, 0.5),
                    ),
                ],
            ];

            let to_move = self
                .entities
                .iter()
                .enumerate()
                .filter_map(|(i, (_, p, s))| {
                    let mut first: Option<(usize, usize)> = None;
                    let mut multiple = false;
                    for x in 0..2 {
                        for y in 0..2 {
                            let q = quad_bounds[x][y];
                            if q.contains((*p, *s)) {
                                if first.is_some() {
                                    multiple = true;
                                }
                                first = Some((x, y))
                            }
                        }
                    }
                    if let Some((p, s)) = first {
                        if !multiple {
                            Some((i, p, s))
                        } else {
                            None
                        }
                    } else {
                        None
                    }
                })
                .collect::<Vec<_>>();

            if to_move.len() > 1 {
                let mut quads = [
                    [
                        box Quad::at(quad_bounds[0][0]),
                        box Quad::at(quad_bounds[0][1]),
                    ],
                    [
                        box Quad::at(quad_bounds[1][0]),
                        box Quad::at(quad_bounds[1][1]),
                    ],
                ];
                for (i, x, y) in to_move {
                    let (e, p, s) = self.entities.swap_remove(i);
                    quads[x][y].add_inner(e, p, s)
                }
                self.quads = Some(quads)
            }
        }
    }

    fn remove_inner(&mut self, ent: Entity, _pos: Position, _size: Size) {
        self.entities.retain(|(e, _p, _s)| *e != ent)
        // TODO subdivide
    }

    pub fn query_overlap<'a>(
        &'a self,
        bounds: impl Bounds + 'a + Copy,
    ) -> Box<dyn Iterator<Item = Entity> + '_> {
        if !self.bounds.overlaps(bounds) {
            return box [].into_iter();
        }
        // TODO subdivide
        box self
            .entities
            .iter()
            .filter(move |(_e, p, s)| (*p, *s).overlaps(bounds))
            .map(|e| e.0)
    }
}

pub fn get_bounds(world: &hecs::World, e: Entity) -> (Position, Size) {
    (
        world
            .get::<&Position>(e)
            .ok()
            .map(|e| **e)
            .unwrap_or(Position(0.0, 0.0)),
        world
            .get::<&Size>(e)
            .ok()
            .map(|e| **e)
            .unwrap_or(Size(0.0, 0.0)),
    )
}
