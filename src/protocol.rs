use crate::{
    features::tilemap::{TileChunk, Tilemap},
    Camera, Dialog, Name, Parented, Player, Position, Size, Texture,
};
use serde::{Deserialize, Serialize};
use std::{any::TypeId, fmt::Debug};

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum ClientboundPacket {
    Info { player_id: u32 },
    UpdateEntity { id: u32, data: serde_json::Value },
    DestroyEntity { id: u32 },
    Error { data: String },
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum ServerboundPacket {
    Login { name: String },
    Quit,
    Movement { x: f64, y: f64 },
    Chat { message: String },
}

fn fmt<'a, T: hecs::Component + Serialize>(
    entity: hecs::EntityRef<'_>,
) -> Option<serde_json::Value> {
    Some(serde_json::to_value(&*entity.get::<T>()?).ok()?)
}

// TODO when more components exist, use more efficient structure.
const FUNCTIONS: &[(
    &'static str,
    &dyn Fn(hecs::EntityRef<'_>) -> Option<serde_json::Value>,
    TypeId,
)] = &[
    ("position", &fmt::<Position>, TypeId::of::<Position>()),
    ("size", &fmt::<Size>, TypeId::of::<Size>()),
    ("texture", &fmt::<Texture>, TypeId::of::<Texture>()),
    ("player", &fmt::<Player>, TypeId::of::<Player>()),
    ("name", &fmt::<Name>, TypeId::of::<Name>()),
    ("camera", &fmt::<Camera>, TypeId::of::<Camera>()),
    ("dialog", &fmt::<Dialog>, TypeId::of::<Dialog>()),
    ("parented", &fmt::<Parented>, TypeId::of::<Parented>()),
    ("tilemap", &fmt::<Tilemap>, TypeId::of::<Tilemap>()),
    ("tilechunk", &fmt::<TileChunk>, TypeId::of::<TileChunk>()),
];

pub fn proto_component_name(c: TypeId) -> &'static str {
    FUNCTIONS.into_iter().find(|(_, _, e)| e == &c).unwrap().0
}

pub fn is_serializable(t: TypeId) -> bool {
    FUNCTIONS.into_iter().find(|(_, _, e)| e == &t).is_some()
}

pub fn serialize_entity(
    entity: hecs::EntityRef<'_>,
    component: Option<TypeId>,
) -> serde_json::Value {
    serde_json::Value::Object(serde_json::Map::from_iter(FUNCTIONS.iter().filter_map(
        |(n, f, t)| {
            if component.map_or(true, |t2| t2 == *t) {
                Some((n.to_string(), f(entity)?))
            } else {
                None
            }
        },
    )))
}

// TODO use this instead of hardcoding
// fn struct_name<'de, T>() -> &'static str
// where
//     T: Deserialize<'de>,
// {
//     struct StructFieldsDeserializer<'a> {
//         name: &'a mut Option<&'static str>,
//     }

//     impl<'de, 'a> Deserializer<'de> for StructFieldsDeserializer<'a> {
//         type Error = serde::de::value::Error;

//         fn deserialize_any<V>(self, _visitor: V) -> Result<V::Value, Self::Error>
//         where
//             V: Visitor<'de>,
//         {
//             Err(serde::de::Error::custom("I'm just here for the name"))
//         }

//         fn deserialize_struct<V>(
//             self,
//             name: &'static str,
//             _fields: &'static [&'static str],
//             visitor: V,
//         ) -> Result<V::Value, Self::Error>
//         where
//             V: Visitor<'de>,
//         {
//             println!("{} {:?}", name, _fields);
//             *self.name = Some(name);
//             self.deserialize_any(visitor)
//         }

//         forward_to_deserialize_any! {
//             bool i8 i16 i32 i64 u8 u16 u32 u64 f32 f64 char str string bytes
//             byte_buf option unit unit_struct newtype_struct seq tuple
//             tuple_struct map enum identifier ignored_any
//         }
//     }

//     let mut fields = None;
//     let _ = T::deserialize(StructFieldsDeserializer { name: &mut fields });
//     fields.unwrap()
// }
