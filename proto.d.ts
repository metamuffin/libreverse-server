// deno-lint-ignore-file no-explicit-any

export type ID = string
export type ClientboundPacket = Info | UpdateEntity | DestroyEntity | Error
export type ServerboundPacket = Login | Movement | ClickEntity | Error | Chat

export interface Login {
    type: "login"
    name: string
}

export interface Info {
    type: "info",
    player_id: ID
}

export interface UpdateEntity {
    type: "update_entity"
    id: ID
    data: { [key: string]: any }
}

export interface DestroyEntity {
    type: "destroy_entity"
    id: ID
}

export interface ClickEntity {
    type: "click_entity",
    id: ID
}

export interface Movement {
    type: "movement"
    x: number
    y: number
}

export interface Error {
    type: "error",
    data: string
}

export interface Chat {
    type: "chat",
    message: string
}
