use std::{io::Write, net::TcpStream};

use bencher::Bencher;
use libreadventure::protocol::ServerboundPacket;

fn connect(b: &mut Bencher) {
    b.iter(|| {
        let mut conn = TcpStream::connect("127.0.0.1:6969").unwrap();
        conn.write_fmt(format_args!(
            "{}\n",
            serde_json::to_string(&ServerboundPacket::Login {
                name: "test".to_string(),
            })
            .unwrap(),
        ))
        .unwrap();
        drop(conn)
    })
}

bencher::benchmark_group!(all, connect);
bencher::benchmark_main!(all);
